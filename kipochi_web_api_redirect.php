<?php
    //Get and App and username in Kipochi to get an API KEY
    
    $username="APP_USERNAME";
    
    $apiKey = "API_KEY"; 
    
    
    $gateway_url = "https://hela.kipochi.io/api/v1/kpg.php";//Kipochi Gateway URL
    
    $amount ="XXX"; //Amount to be transacted
   
    $success_url = "SUCCESS_CALLBACK_URL"; //URL called when the transactio is successful
    
    
    $fail_url = "FAILURE_CALLBACK_URL"; //URL called when the transaction is unsuccessful


    $accountName="PAYBILL_ACCOUNT_NO";//set the paybill account no.

    $pesalinkNo="07XX";//set pesalink phone number connected to ncba bank account with registered push notification callback to hela.kipochi.io/api/v1/ncba_callback.php

    
    // Set any metadata that you would like to send along with this request.
    // ONLY change the values XXX and YYY.
    
    $extra1="XXX";//Add your own extra data alongside your request
    
    $extra2="YYY";//Add your own extra data alongside your request
    $dataArray = array(
        'username'    =>$username,
        'apikey'      =>$apiKey,
        'amount'      =>$amount,
        'pesalinkNo'  =>$pesalinkNo,
        'accountName' =>$accountName,
        'success_url' =>$success_url,
        'fail_url'    =>$fail_url,
        'extra1'   => $extra1,
        'extra2'   => $extra2,
        
        ); 
  
  
     $data = http_build_query($dataArray);
     
     $redirect_src=$gateway_url."?".$data;

    //redirect to kipochi gateway
 
?>
  
<!--Place the redirect below inside your body tag-->

<meta http-equiv="Refresh" content="0; url='<?php echo $redirect_src; ?>' />