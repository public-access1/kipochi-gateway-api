//SUCCESSFUL RESPONSE

{
    "response":{
        "status":"success",
        "shortcode":"1234567",//Paybill no.
        "amount":"1000.00", //Transaction Amount
        "reference":"OJJXXX", //M-Pesa reference no.
        "accountName":"XXX",
        "phoneNumber":"2547XX", //Payer phone Number
        "username":"JOHN DOE", //Name of payer
        "transtime":"20201019175929", //Format YYYYMMDDHHmmss
        "extra1":"",
        "extra2":"",
        "identifier":"MPESA"
    }
}

//FAILURE RESPONSE

{
    "response":{
        "status":"failed",
        "shortcode":"1234567",//Paybill no.
        "phoneNumber":"2547XXX", //Phone number of user that cancelled
        "resultCode":1234,
        "resultDesc":"Request cancelled by user",//Description of failure of transaction
        "extra1":"",
        "extra2":"",
        "identifier":"MPESA"
    }
}